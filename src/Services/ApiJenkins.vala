using Soup;
using Json;

namespace Majster.Services.ApiJenkins {
    private string auth_string;
    private string auth_headers;
    private string api_endpoint;
    private string api_url;
    private const string SCHEMA_NAME = "com.github.oskarkunik.majster";

    struct Credentials {
        public string url;
        public string username;
        public string token;
    }

    public async bool get_data (string url, string username, string token, string data) {
        message ("\nurl: %s\nusername: %s\ntoken: %s\npath: %s", url, username, token, data);
        
        auth_string = @"$username:$token";
        auth_headers = @"Basic $auth_string";

        switch (data) {
            case "auth":
            case "panel":
                api_endpoint = "job/panel";
                break;
            default:
                api_endpoint = "job/panel";
                break;
        }

        api_url = @"https://$auth_string@$url/$api_endpoint/api/json";

        // Create a session
        Soup.Session session = new Soup.Session ();
        // Add a logger
        Soup.Logger logger = new Soup.Logger (Soup.LoggerLogLevel.MINIMAL, -1);	
        session.add_feature (logger);
        // Send a request
        Soup.Message msg = new Soup.Message ("GET", api_url);
        msg.request_headers.append ("Authorization", auth_headers);
        session.send_message (msg);

        //  stdout.printf ("\ncode: %u\n", msg.status_code);
        //  print ("Data: \n%s\n", (string) msg.response_body.data);

        var result = false;
        var store_loop = new MainLoop ();

        if (msg.status_code == 200) {
            store_credentials.begin (url, username, token, (obj, res) => {
                    result = store_credentials.end (res);
                    store_loop.quit ();
                });
            return result;
        } else {
            result = false;
            store_loop.quit ();
            return result;
        }

        store_loop.run ();
        return result;
    }

    // Save user credentials
    public async bool store_credentials (string url, string username, string token) {
        var schema = new Secret.Schema (SCHEMA_NAME, Secret.SchemaFlags.NONE);
        var credentials = @"{\"url\":\"$url\",\"username\":\"$username\",\"token\":\"$token\"}";
        var result = false;
        var loop = new MainLoop ();
        
        Secret.password_store.begin (schema, Secret.COLLECTION_DEFAULT, "Majster", credentials, null, (obj, async_res) => {
            result = Secret.password_store.end (async_res);
            loop.quit();
        });
        
        loop.run ();
        return result;
    }

    public async bool clear_credentials () {
        var schema = new Secret.Schema (SCHEMA_NAME, Secret.SchemaFlags.NONE);
        var loop = new MainLoop ();
        var cleared = false;

        Secret.password_clear.begin (schema, null, (obj, async_res) => {
            cleared = Secret.password_clear.end (async_res);
            loop.quit();
        });

        loop.run ();
        return cleared;
    }

    // Check if credentials are already saved in the system
    public async string get_credentials (Gtk.Stack stack) {
        var schema = new Secret.Schema (SCHEMA_NAME, Secret.SchemaFlags.NONE);
        string credentials = "";
        var loop = new MainLoop ();

        Secret.password_lookup.begin (schema, null, (obj, async_res) => {
            credentials = Secret.password_lookup.end (async_res);
            loop.quit();
        });

        loop.run ();
        return credentials;
    }

    public static void parse_credentials (string credsJson) {
        var parser = new Json.Parser ();
        parser.load_from_data ((string) credsJson);
        var node = parser.get_root ();
        Json.Reader reader = new Json.Reader (node);
        string url = null;
        string username = null;
        string token = null;

        foreach (string member in reader.list_members ()) {
            switch (member) {
                case "url":
                    bool tmp = reader.read_member ("url");
                    assert (tmp == true);
                    assert (reader.is_value ());
        
                    url = reader.get_string_value ();
                    reader.end_member ();
                    break;
        
                    case "username":
                    bool tmp = reader.read_member ("username");
                    assert (tmp == true);
                    assert (reader.is_value ());
        
                    username = reader.get_string_value ();
                    reader.end_member ();
                    break;

                    case "token":
                    bool tmp = reader.read_member ("token");
                    assert (tmp == true);
                    assert (reader.is_value ());
        
                    token = reader.get_string_value ();
                    reader.end_member ();
                    break;
        
                default:
                    assert_not_reached ();
                }
        }

        print ("\n");
        print (url);
        print ("\n");
        print (username);
        print ("\n");
        print (token);
        print ("\n");

    }

}