using Gtk;
using Granite;
using Granite.Services;

namespace Majster {
    public class MainWindow : Gtk.Window {
        public Widgets.Headerbar header;
        public Widgets.Login login_screen;
        public Widgets.Projects projects_screen;
        private Granite.Widgets.Welcome welcome_screen;
        public static Gtk.Stack stack;
        private Gtk.Overlay overlay;
        private Gtk.Grid grid;

        public MainWindow (Gtk.Application application) {
            Object (application: application,
                    icon_name: "com.github.oskarkunik.majster",
                    resizable: true,
                    title: _("Majster"),
                    default_height: 400,
                    default_width: 600);
        }

        construct {
            header = new Widgets.Headerbar (this);
            header.title = this.title;
            header.set_subtitle (_("A Jenkins control app"));
            this.set_titlebar (header);
            stack = new Gtk.Stack ();
            var loop = new MainLoop ();
            var credentials = "";

            grid = new Gtk.Grid ();
            grid.orientation = Gtk.Orientation.VERTICAL;

            build_welcome_screen ();
            build_login_screen ();

            stack.add_named (welcome_screen, "welcome");
            stack.add_named (login_screen, "login");
            
            Services.ApiJenkins.get_credentials.begin (stack, (obj, async_res) => {
                credentials = Services.ApiJenkins.get_credentials.end (async_res);
                Services.ApiJenkins.parse_credentials (credentials);
                print (credentials);
                if (credentials.char_count () > 0) {
                    if (stack.get_child_by_name ("projects") == null) {
                        build_projects_screen ("url", "username", "token");
                        stack.add_named (projects_screen, "projects");
                    }
                    stack.visible_child_name = "projects";
                } else if (credentials.char_count () == 0) {
                    stack.visible_child_name = "welcome";
                }
                loop.quit (); 
            });
            
            loop.run ();
            
            grid.add (stack);

            overlay = new Gtk.Overlay ();
            overlay.add_overlay (grid);

            add (overlay);

            this.destroy.connect (Gtk.main_quit);
        }

        public static void clear_credentials () {
            var loop = new MainLoop ();
            Services.ApiJenkins.clear_credentials.begin ((obj, async_res) => {
                var credentials = Services.ApiJenkins.clear_credentials.end (async_res);
                loop.quit (); 
            });
            stack.visible_child_name = "welcome";
            
            loop.run ();
        } 

        private void build_login_screen () {
            login_screen = new Widgets.Login (stack);
        }

        private void build_projects_screen (string url, string username, string token) {
            projects_screen = new Widgets.Projects (url, username, token);
        }

        private void build_welcome_screen () {
            welcome_screen = new Granite.Widgets.Welcome (_("Welcome to Majster"), _("Add an account to continue."));

            welcome_screen.append ("dialog-password", _("Add Jenkins account"), _("Connect to server using your username and password."));
        
            welcome_screen.activated.connect ((index) => {
                switch (index) {
                    case 0:
                        on_login ();
                    break;
                default:
                    break;
                }
            });

        }
    
        private void on_login () {
            stack.visible_child_name = "login";
            return;
        }

        public override bool configure_event (Gdk.EventConfigure event) {
            int root_x, root_y;
            get_position (out root_x, out root_y);
            Majster.Application.settings.set_int ("window-x", root_x);
            Majster.Application.settings.set_int ("window-y", root_y);
    
            return base.configure_event (event);
        }
    }
}