namespace Majster.Widgets {
    public class ProjectRow : Gtk.ListBoxRow {
        private Gtk.Label project_name;

        public ProjectRow () {
            var grid = new Gtk.Grid ();
            grid.margin = 12;
            grid.margin_bottom = grid.margin_top = 6;
            grid.column_spacing = 12;
            grid.row_spacing = 3;
    
            add (grid);

            project_name = new Gtk.Label ("Test");
            project_name.halign = Gtk.Align.START;
            project_name.get_style_context ().add_class ("h3");
            grid.attach (project_name, 0, 0, 1, 1);
        }

    }

}