namespace Majster.Widgets {
    public class Preferences : Gtk.Dialog {
        public Preferences (Gtk.Window? parent) {
            Object (
                border_width: 6,
                deletable: false,
                resizable: false,
                default_height: 200,
                default_width: 300,
                title: _("Preferences"),
                transient_for: parent,
                destroy_with_parent: true,
                window_position: Gtk.WindowPosition.CENTER_ON_PARENT
            );
        }

        construct {
            var main_stack = new Gtk.Stack ();
            main_stack.margin = 12;
            main_stack.margin_top = 0;

            var reset_btn = new Gtk.Button.with_label (_("Delete saved credentials"));
            main_stack.add (reset_btn);
            ((Gtk.Button) reset_btn).clicked.connect (() => {
                this.destroy ();
                MainWindow.clear_credentials ();
            });

            var close_btn = add_button (_("Close"), Gtk.ResponseType.CLOSE);
            ((Gtk.Button) close_btn).clicked.connect (() => destroy ());

            var main_grid = new Gtk.Grid ();
            main_grid.margin_top = 0;
            main_grid.attach (main_stack, 0, 0, 1, 1);

            ((Gtk.Container) get_content_area ()).add (main_grid);
}
    }
}