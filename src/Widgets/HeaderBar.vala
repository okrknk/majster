namespace Majster.Widgets {
    public class Headerbar : Gtk.HeaderBar {
        public MainWindow win;
        private Gtk.Button menu_btn;

        public Headerbar (MainWindow win) {
            this.win = win;
            var header_context = this.get_style_context ();

            create_ui ();
            header_context.add_class ("majster-headerbar");
        }

        private void create_ui () {
            var preferences = new Gtk.ModelButton ();
            preferences.text = (_("Preferences"));

            menu_btn = new Gtk.Button ();
            menu_btn.image = new Gtk.Image.from_icon_name ("open-menu", Gtk.IconSize.LARGE_TOOLBAR);
            menu_btn.has_tooltip = true;
            menu_btn.tooltip_text = (_("Settings"));

            menu_btn.clicked.connect (() => {
                show_preferences ();
            });

            pack_end (menu_btn);

            set_show_close_button (true);
            this.show_all ();
        }

        public void show_preferences () {
            var dialog = new Widgets.Preferences (win);
            dialog.set_modal (true);
            dialog.show_all ();
        }
    }
}