using Granite;

namespace Majster.Widgets {
    public class Login : Gtk.Grid {
        private Gtk.Label url_label;
        private Gtk.Entry url;
        private Gtk.Label username_label;
        private Gtk.Entry username;
        private Gtk.Label token_label;
        private Gtk.Entry token;
        private Gtk.Button connect_btn;
        private Gtk.Button cancel_btn;
        private Granite.Widgets.OverlayBar overlay_bar;
        private Gtk.InfoBar bar;
        private Gtk.Box input_box;


        public Login (Gtk.Stack stack) {
            GLib.Object (orientation: Gtk.Orientation.VERTICAL);
            build_inputs (stack);
        }

        private void build_inputs (Gtk.Stack stack) {

            Gtk.Entry url = new Gtk.Entry ();
            Gtk.Entry username = new Gtk.Entry ();
            Gtk.Entry token = new Gtk.Entry ();

            url_label = new Gtk.Label (_("Jenkins instance URL"));
            username_label = new Gtk.Label (_("Jenkins user name"));
            token_label = new Gtk.Label (_("Jenkins user token"));

            connect_btn = new Gtk.Button.with_label ("Connect");
            cancel_btn = new Gtk.Button.with_label ("Cancel");

            bar = new Gtk.InfoBar ();
            bar.set_message_type (Gtk.MessageType.ERROR);
            bar.set_revealed (false);
            Gtk.Container content = bar.get_content_area ();
            content.add (new Gtk.Label ("Could not connect to Jenkins API. Please check your credentials."));

            input_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);

            overlay_bar = new Granite.Widgets.OverlayBar ();
            overlay_bar.label = "Connecting";
            overlay_bar.active = true;
            
            input_box.add (url_label);
            input_box.add (url);
            input_box.add (username_label);
            input_box.add (username);
            input_box.add (token_label);
            input_box.add (token);
            input_box.add (connect_btn);
            input_box.add (cancel_btn);

            this.attach (bar, 0, 0, 3, 1);
            this.attach (input_box, 1, 1, 1, 1);

            connect_btn.clicked.connect (() => {
                unowned string url_str = url.get_text ();
                unowned string username_str = username.get_text ();
                unowned string token_str = token.get_text ();
                var authorized = false;
                var loop = new MainLoop ();
                this.attach (overlay_bar, 0, 0, 1, 1);

                Services.ApiJenkins.get_data.begin (url_str, username_str, token_str, "auth", (obj, async_res) => {
                        authorized = Services.ApiJenkins.get_data.end(async_res);
                        if (authorized == true) {
                            if (stack.get_child_by_name ("projects") == null) {
                                var projects_screen = new Widgets.Projects ("url", "username", "token");
                                stack.add_named (projects_screen, "projects");
                            }
                            stack.visible_child_name = "projects";
                        } else {
                            bar.set_revealed (true);                            
                        }
                        this.remove (overlay_bar);
                        loop.quit ();
                });
                loop.run ();   
                return;
            });
            
            cancel_btn.clicked.connect (() => {
                stack.visible_child_name = "welcome";
            });

        }

    }
}