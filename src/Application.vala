namespace Majster {

    public class Application : Gtk.Application {
        public static GLib.Settings settings;
        public Widgets.Headerbar header;
        public static MainWindow win = null;
   
        construct {
            application_id = "com.github.oskarkunik.majster";
            flags = ApplicationFlags.FLAGS_NONE;
            settings = new Settings ("com.github.oskarkunik.majster");
        }

        protected override void activate () {
            new_win ();
        }

        public void new_win () {
            if (win != null) {
                win.present ();
                return;
            }
            win = new MainWindow (this);

            var window_x = settings.get_int ("window-x");
            var window_y = settings.get_int ("window-y");

            if (window_x != -1 ||  window_y != -1) {
                win.move (window_x, window_y);
            }

            win.show_all ();
        }

        public static int main (string[] args) {
            var app = new Majster.Application ();
            return app.run (args);
        }
    }

}